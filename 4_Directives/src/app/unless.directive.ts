import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
  //Quasi ngIf umkehren
  //Bei ngIf (Falls True anzeigen)
  //Hier umgekehrt
  @Input('appUnless') set unless(condition: boolean) {
    if(condition) {
      this.vcRef.clear(); //Bestehendes Element wird gelöscht (zb Div)
    } else {
      this.vcRef.createEmbeddedView(this.templateRef); //Neue View erstellen
    }
  }
  //Alias ('appUnless') damit es von außen zugreifbar ist
  //Ohne Alias dann  @Input() set unless()

  constructor(private templateRef: TemplateRef<any>, private vcRef: ViewContainerRef) { }

}
