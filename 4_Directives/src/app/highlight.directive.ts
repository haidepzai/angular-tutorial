import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2 } from '@angular/core';
/*
'[appHighlight]' hat nichts mit Property Binding zu tun, 
sondern ist ein CSS Selector und selektiert alle Elemente, 
die <div appHighlight> heißen
*/
@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit {
  flag: boolean = true;
  @Input() defaultColor = 'white';
  @Input() highlightColor = 'green';

  @HostBinding('style.backgroundColor') farbe;
  //Direkt auf das Element zugreifen mit HostBinding
  @HostBinding('innerText') text = "Hallo";
  @HostBinding('attr.class') cssClass = "someClass";

  @HostListener('mouseenter') mouseenter() {
    this.farbe = this.highlightColor;
  }
  //Zugriff auf Event vom Element
  @HostListener('mouseleave') mouseleave() {
    this.farbe = this.defaultColor;
  }
  @HostListener('click') onClick() {
    this.flag = !this.flag;
    this.flag === true ? this.cssClass = "someClass" : this.cssClass = null;
  }


  // constructor(elRef: ElementRef, renderer: Renderer2) { 
  //   //elRef.nativeElement.style.backgroundColor = "green";
  //   renderer.setStyle(elRef.nativeElement, 'background-color', 'green');
  // }

  ngOnInit() {
    setTimeout(() => {
      this.farbe = 'red';
    }, 3000);

    this.farbe = this.defaultColor;
  }

}
