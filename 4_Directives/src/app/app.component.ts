import { Component } from '@angular/core';

//<app-root></app-root>
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  color = 'red';
  switch = true;
  elemente = [1,2,3,4,5];
  value: string = '10';

  test= [
    {name: "Otto", number: 1},
    {name: "Nest", number: 2},
    {name: "Dennis", number: 3},
    {name: "Julia", number: 4},
  ];
  testObject: { [key: number]: string } =
  {
    1: 'Object Value 1',
    2: 'Object Value 2',
    3: 'Object Value 3'
  };
  testMap = new Map([
    [2, 'Map Value 2'],
    [null, 'Map Value 3'],
    [1, 'Map Value 1'],
  ]);
  

  constructor(){
    setTimeout(() => {
      this.color = 'green';
    }, 3000);
  }
}
