import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  text = "irgendein text";
  date = new Date();
  list = ['Brot', 'Milch', 'Honig', 'Vollkornbrot'];
  newArray = [];

  asyncValue = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Bin da!');
    }, 2000);
  });

  //Bei Pipe ist pure: false Bad Practice (Performance)
  //Deswegen neuen Array erstellen, die Werte vom alten übernehmen und neues Element pushen
  onClick(item: string){
    //this.newArray.concat(this.list);
    this.newArray = [...this.list]; //Werte kopieren
    this.newArray.push(item);
    this.list = [...this.newArray];
    this.newArray = [];
  }
}
