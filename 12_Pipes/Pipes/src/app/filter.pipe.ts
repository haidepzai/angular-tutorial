import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false //pure: false dient dazu, dass die Pipe immer neu berechnet, 
                //falls sich was verändert (jedoch Bad Practice)

})
export class FilterPipe implements PipeTransform { 
    //value ist Array, args in dem Fall der zu filternde Begriff
    transform(value: any, args?: any){
        //falls Array Länge leer ist, einfach leere Array zurückgeben
        if (value.length === 0){
            return value;
        }
        let resultArray = [];
        for (let item of value){
            if(item.match('^.*' + args + '.*$')){
                resultArray.push(item);
            }
        }
        return resultArray;
    }
}