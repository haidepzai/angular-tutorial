import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  template: `
  <div class="row">
    <div class="col-xs-12">
      <h2>Dein Account</h2>
      <p>Ich verändere mich nur, wenn Component neu geladen wird: {{snapID}}</p>
      <p>Ich verändere mich bei jeder Aktualisierung: {{id}}</p>
      <button class="btn btn-primary" (click)="onNavigate()">Zur Startseite</button>
      <hr>
      <ul class="nav navbar-nav">
        <li><a [routerLink]="['detail']">Detail</a></li>
        <li><a [routerLink]="['edit']">Edit</a></li>
      </ul>       
    </div>
  </div>
  <hr>
  <router-outlet><!-- Hier sind die Child Routes drin --></router-outlet>
  `,
  styles: [
  ]
})
export class UserComponent implements OnInit, OnDestroy {
  snapID: string;
  id: string;
  private subscription: Subscription;


  /**
   * 
   * @param router Stellt Router zu Verfügung, sodass wir im Code routing verwenden können
   * @param activatedRoute Gibt Info zu aktivierten Route zu geben
   */
  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  //Wird aufgerufen, wenn es zum 1. Mal erstellt wird
  ngOnInit(): void {
    //Wird nur aktualisiert, wenn Component neu geladen wird
    this.snapID = this.activatedRoute.snapshot.params['id']; //Gibt aktuellen Parameter
    //Besser:
    this.subscription = this.activatedRoute.params.subscribe(
      (params: Params) => this.id = params['id']
    ); //Observable
  }
  //Durch subscribe hört man sozusagen zu und weiß, wann immer sich der Parameter ändert

  /**
   * @param queryParams: im URL ?token=100
   * Beim Klick des Buttons wird die Methode aufgerufen
   * Navigiert zu Home und übergibt ein queryParams im 2. Parameter
   * @param fragment: im URL #anchor
   * Springt dann zu dem Abschnitt mit der ID="anchor"
   */
  onNavigate() {
    this.router.navigate(['/'], {queryParams: {'token': 100}, fragment: 'anchor1'});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe(); 
    //Wann immer die Component zerstört wird -> unsubscriben, sonst Memory Leak
  } //Jedopch seit neue Version nicht mehr nötig, da Angular das selbst übernimmt!

}
