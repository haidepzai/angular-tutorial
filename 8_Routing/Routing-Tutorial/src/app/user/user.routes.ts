import { UserEditGuard } from './user-edit.guard';
import { UserDetailGuard } from './user-detail.guard';
import { UserEditComponent } from './user-edit.component';
import { UserDetailComponent } from './user-detail.component';
import { Routes } from '@angular/router';

//Verschachtelte Routes (die nach /user/:id/ kommen)
//Idee ist, wir wollen verschachtelt laden, dass user-detail in user.component ist
//Lösung: Child Routes (muss bei app.routing definiert sein bei UserComponent)
export const USER_ROUTES: Routes = [
    //canActivate Guard (Route vor Zugriff "schützen"): User wird davor gefragt beim Öffnen
    { path: 'detail', component: UserDetailComponent, canActivate: [UserDetailGuard]}, //user/:id/detail
    { path: 'edit', component: UserEditComponent, canDeactivate: [UserEditGuard]} //user/:id/edit
    //canDeactivate Guard (Route vor dem Verlassen "schützen"): UserEditGuard muss auch im app.module provided werden!
];