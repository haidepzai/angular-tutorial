import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";

//Interface that a class can implement to be a guard deciding if a route can be activated. 
export class UserDetailGuard implements CanActivate {
    //Rückgabetyp ist ein Boolean
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        console.log(route.url);
        console.log(route.url[0].path);
        return confirm('Sicher?'); //Ja/Nein (boolean)
    }
}
