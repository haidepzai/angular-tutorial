import { CanDeactivate } from "@angular/router";
import { Observable } from "rxjs";

//Alle Components, die canDeactivate Funktion benutzen möchten,
//müssen dieses Interface implementieren
export interface ComponentCanDeactivate {
    CanDeactivate: () => boolean | Observable<boolean>;
}

/*
Interface that a class can implement to be a guard deciding if a route can be deactivated. 
If all guards return true, navigation continues. If any guard returns false, navigation is cancelled.
*/
export class UserEditGuard implements CanDeactivate<ComponentCanDeactivate> {
    //Erwartet ein Component und liefert ein Boolean
    canDeactivate(component: ComponentCanDeactivate): Observable<boolean> | boolean {
        //return component.saved (von user-edit.component) geht hier nicht, da es jede component sein kann
        return component.CanDeactivate(); //true or false (Beim Verlassen eine Component wird diese Funktion aufgerufen: Funktion wurde ja implementiert)
        //Solange false, kann man Component nicht verlassen
        //CanDeactivate(); ist die Methode vom Interface und canDeactivate erwartet ein component mit dem Interface ComponentCanDeactivate
    }
}