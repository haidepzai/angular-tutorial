import { ComponentCanDeactivate } from './user-edit.guard';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-edit',
  template: `
    <div class="row">
      <div class="col-xs-12">
        <p>Möchtest du deinen Account bearbeiten?</p>
        <button class="btn btn-primary" (click)="onSave()">Speichern</button>
      </div>    
    </div>
  `,
  styles: [
  ]
})
export class UserEditComponent implements OnInit, ComponentCanDeactivate {
  saved: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  onSave() {
    this.saved = true;
  }

  //Guard wird jedes Mal aufgerufen, wenn man versucht diese component zu verlassen
  //Weil im user.routes canDeactivate definiert wurde!
  //Diese CanDeactivate Funktion wird vom Guard aufgerufen!! (return component.CanDeactivate)
  CanDeactivate() {
    console.log(this.saved);
    return !this.saved ? confirm('Sicher, wurde nicht gespeichert?') : true ;
    //confirm gibt true/false zurück
  }

}
