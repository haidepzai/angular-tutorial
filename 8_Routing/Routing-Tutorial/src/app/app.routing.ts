import { USER_ROUTES } from './user/user.routes';
import { HomeComponent } from './home.component';
import { UserComponent } from './user/user.component';
import { Routes, RouterModule } from '@angular/router';

const APP_ROUTES: Routes = [
    { path: '', component: HomeComponent}, //Home
    { path: 'user/:id', component: UserComponent, children: USER_ROUTES },
    //:id ist dynamisch; children sind die verschachtelten Routes (zu finden bei user.routs.ts)
    { path: '**', redirectTo: '/'} //wildcard (default) muss am Ende stehen

];
//routing muss in app.module bei imports drinstehen!!
export const routing = RouterModule.forRoot(APP_ROUTES);