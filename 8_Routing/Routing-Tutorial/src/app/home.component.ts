import { Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-home',
  template: `
  <div class="row">
    <div class="col-xs-12">
      <h2>Home</h2>
      <p *ngIf="token !== undefined">Token: {{token}}</p>
      <p *ngIf="fragment != undefined">Fragment: {{fragment}}</p>
    </div>
  </div>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private subscriptionFragment: Subscription;
  token: string;
  fragment: string;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    //this.token = this.activatedRoute.snapshot.queryParams['token'];
    this.subscription = this.activatedRoute.queryParams.subscribe(
      (queryParams: Params) => this.token = queryParams['token']
    );//Selektiert den Query Parameter vom Link und weist sie auf this.token
    //Lauscht sozusagen den queryParams und aktualisiert this.token jedes Mal
    this.subscriptionFragment = this.activatedRoute.fragment.subscribe(
      fragment => this.fragment = fragment
    );
  } 

  ngOnDestroy(){
    this.subscription.unsubscribe(); //optional (Best Practice)
    this.subscriptionFragment.unsubscribe();
  }

}
