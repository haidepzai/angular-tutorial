import { Component, ContentChild, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.css']
})
export class OtherComponent implements OnInit {
  @ViewChild('i') input: ElementRef;
  //Zugriff auf #input im Template
  //Zugriff nur auf diese Component/Template#
  
  @ContentChild('paragraph') paragraph: ElementRef;
  //Zugriff die im Nachhinein übergeben wurden
  //Also nicht in diesem Template
  //Zb mit ng-content

  platzhalter: string = "Hallo";

  constructor() {
    setTimeout(() => {
      this.input.nativeElement.value = "Wert überschrieben!";
      this.paragraph.nativeElement.innerText = "Wert überschrieben!"
      this.platzhalter = "Wert überschrieben!"
    }, 3000)
   }

  ngOnInit(): void {
  }

}
