// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD9obbjKQOyPju2gRbRWPOb9qxrrwttwGY",
    authDomain: "authentification-tutorial.firebaseapp.com",
    databaseURL: "https://authentification-tutorial.firebaseio.com",
    projectId: "authentification-tutorial",
    storageBucket: "authentification-tutorial.appspot.com",
    messagingSenderId: "477141983183",
    appId: "1:477141983183:web:7f4432f4d9556bd63efcba",
    measurementId: "G-2M89PW5YY4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
