import { ProtectedComponent } from './protected/protected.component';
import { SignupComponent } from './unprotected/signup.component';
import { SigninComponent } from './unprotected/signin.component';
import { HeaderComponent } from './shared/header.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { routing } from './app.routing';
import { AuthService } from './shared/auth.service';
import { ProtectedGuard } from './protected/protected.guard';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SigninComponent,
    SignupComponent,
    ProtectedComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    routing
  ],
  providers: [AuthService, ProtectedGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
