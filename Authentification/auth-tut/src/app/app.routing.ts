import { ProtectedComponent } from './protected/protected.component';
import { SigninComponent } from './unprotected/signin.component';
import { SignupComponent } from './unprotected/signup.component';
import { RouterModule, Routes } from "@angular/router";
import { ProtectedGuard } from './protected/protected.guard';

const APP_ROUTES: Routes = [
    {path: '', redirectTo: 'signup', pathMatch: 'full'},
    {path: 'signup', component: SignupComponent},
    {path: 'signin', component: SigninComponent},
    {path: 'protected', component: ProtectedComponent, canActivate: [ProtectedGuard]}
]

export const routing = RouterModule.forRoot(APP_ROUTES);