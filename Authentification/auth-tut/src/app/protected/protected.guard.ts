import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { first } from 'rxjs/operators';


@Injectable()
export class ProtectedGuard implements CanActivate {

    constructor(private authService: AuthService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        //isAuthenticated ist ein Observable, der Wert kann sich ändern
        //Guard erwartet ein Observable, dass ein Wert zwar noch nicht da ist,
        //aber sich nie verändern kann
        return this.authService.isAuthenticated().pipe(
            first()
        );
    }
}