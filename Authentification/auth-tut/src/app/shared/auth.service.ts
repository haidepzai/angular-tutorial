import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { User } from './user.interface';


declare var firebase: any; //declare = globale Variable

@Injectable()
export class AuthService {

    constructor(private router: Router) {}

    signUpUser(user: User) {
        firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
        .then((result) => {
            console.log(result)
        })
        .catch(function(error) {
            console.log(error)
        });
    }

    signInUser(user: User){
        firebase.auth().signInWithEmailAndPassword(user.email, user.password)
            .then((result) => {
                console.log(result)
            })
            .catch(function(error) {
                console.log(error);
            });        
    }

    logout() {
        firebase.auth().signOut(); //Token löschen
        this.router.navigate(['/signin']);
    }

    isAuthenticated() {
        const state = new Subject<boolean>();
        /*onAuthStateChanged(callback) eine Methode die ein Callback erwartet und immer ausgeführt wird, 
        wenn sich der Authentifizierungsstatus ändert (Einloggen/Ausloggen) */
        firebase.auth().onAuthStateChanged(function(user) {
            if(user) { //User loggt sich ein
                state.next(true);
            } else {
                state.next(false); //User loggt sich aus
            }
        });
        return state.asObservable(); //return of(state);
    }
}