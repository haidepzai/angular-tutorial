import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styles: [`
  input.ng-touched.ng-invalid {
    border: 1px solid red;
  }
`
  ]
})
export class ReactiveComponent implements OnInit {
  myForm: FormGroup; //Reactive ist FormGroup statt nguyen
  //ngForm ist sozusagen die Hülle, die automatisch da ist, 
  //wenn Template-Driven Ansatz gewählt wird
  genders = [
    'männlich',
    'weiblich'
  ];

  constructor(private formBuilder: FormBuilder) { }

  onSubmit(){
    console.log(this.myForm.getRawValue());
    this.myForm.reset();
  }

  onAddHobby(){
    //Angular weiß nicht, dass es ein FormArray ist, deswegen <FormArray> davor CASTEN
   (<FormArray>this.myForm.get('hobbies')).push(
     new FormControl('', Validators.required));
  }

  getHobbyControls() {
    return (this.myForm.get('hobbies') as FormArray).controls;
  }
  /* oder so:
  getHobbyControls() {
    return (<FormArray>this.signupForm.get('hobbies')).controls;
  }
  */

  ngOnInit(): void {
    /* this.myForm = new FormGroup({
      //Gruppieren
      'userData': new FormGroup(
        {
          'username': new FormControl(null, Validators.required), 
          'email': new FormControl(null, [
            Validators.required,
            Validators.pattern("[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*")
          ])
        }
      ),
      //FormControl erstellt ein neues Eingabefeld
      'password': new FormControl(null, Validators.required),
      'gender': new FormControl('männlich'), //männlich ist Standard
      'hobbies': new FormArray([
        new FormControl('Cooking', Validators.required, this.asyncExampleValidator)
      ]) //Array aus FormControll
    }); */
    
    //Alternative:
    this.myForm = this.formBuilder.group({
      'userData': this.formBuilder.group({
        'username': this.formBuilder.control("Hai", [
            Validators.required,
            this.exampleValidator
          ]),
        'email': this.formBuilder.control(null, [
          Validators.required,
          Validators.pattern("[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*")
        ])
      }),
      'password': [null, Validators.required], //Kurzschreibweise
      'gender': ['männlich'],
      'hobbies': this.formBuilder.array([
        [null, Validators.required, this.asyncExampleValidator]
      ])
    }); 
    // this.myForm.valueChanges.subscribe(
    //   (data: any) => console.log(data)
    // );
    this.myForm.statusChanges.subscribe(
      (data: any) => console.log(data) //aktuellen Status des ganzen Formulars
    );
  }
  //Eigene Validator
  exampleValidator(control: FormControl): {[key: string]: boolean} {
    if(control.value === 'Example'){
      return {example: true}; //kann true oder false sein
    }
    return null; //nur wenn return null oder nichts ist es bestanden
  }

  asyncExampleValidator(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>(
      (resolve, reject) => {
        setTimeout(() => { //setTimeout um Server Response zu simulieren
          if(control.value === "Example") {
            resolve({asyncExample: true}); //True -> DURCHGEFALLEN
          } else {
            resolve(null);
          }
        }, 1500);
      }
    );
    return promise;
  }
}
