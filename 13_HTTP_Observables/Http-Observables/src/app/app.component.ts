import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HttpService]
})
export class AppComponent {
  users: any = [];
  asyncUsers: Observable<any> = this.httpService.getData();
  //hier ohne subscribe, weil das die async Pipe für mich übernimmt

  constructor(private httpService: HttpService) {}

  onSubmit(username: string, email: string) {
    this.httpService.sendData({
      username: username, email: email
    }).subscribe(
      data => console.log(data),
      error => console.log(error)
    )
  }

  onGetData(){
    this.httpService.getData().subscribe(
      data => this.users = data
    );
    this.httpService.getDatas().subscribe(
      data => console.log(data) //this.users = data
    );
  }
}
