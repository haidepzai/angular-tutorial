import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
    databaseUrl:string = 'https://ng-http-4f212.firebaseio.com/';
    databaseName:string = 'data.json';

    constructor(private http: HttpClient) {}

    sendData(user: any): Observable<any>{
        const body = JSON.stringify(user); //Object zum String
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.http.post(this.databaseUrl + this.databaseName, body, {headers: headers}).pipe(
            catchError((response) => {
                throw throwError(response);
            })
        );
    }

    /*
    map von rxjs vearbeitet Daten. 
    Wenn Daten zurückkommen, 
    dann die Daten von Map nehmen und werden weiter verarbeitet 
    -> gibt ein neues Observable zurück
    */
    getData():Observable<any>{    
        
        return this.http.get(this.databaseUrl + this.databaseName).pipe(
            map((response) => {
                console.log(response);
                console.log(typeof(response));
                const data = response;
                const returnArray = [];
                for(let key in data){
                    console.log(key);
                    console.log(data[key]);
                    returnArray.push(data[key]);
                }
                return returnArray; //Wird automatisch in ein Observable umgewandelt
            })
        )
    }

    getDatas():Observable<any>{    
        
        return this.http.get(this.databaseUrl + this.databaseName);
           
    }

}