import { animate, group, state, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  //trigger besteht aus einem namen und einem Array
  //state besteht aus einem namen und ein style JS Object
  animations: [
    trigger('einfach', [
      state('standard', style({
        'background-color': 'green',
        borderRadius: 0,
        transform: 'translateX(0) scale(1)'
      })),
      state('markiert', style({
        'background-color': 'blue',
        borderRadius: 0,
        transform: 'translateX(100px) scale(1)'
      })),
      state('kleiner', style({
        'background-color': 'blue',
        transform: 'translateX(0) scale(0.5)'
      })),
      //von standard zu markiert und vice versa 300ms
      transition('standard <=> markiert', animate(300)),
      //Wildard * irgendein Status
      transition('* <=> kleiner', [
        style({ //Zwischenphase
          'background-color': 'orange'
        }),
        animate(1000, style({ //Zwischenphase
          'border-radius': '30px'
        })),
        animate(500)
      ]),
    ]),
    trigger('komplex', [
      state('standard', style({
        backgroundColor: 'red',
        opacity: 1
      })),
      transition('standard => void', animate(300)),
      transition('void => standard', [
        style({
          opacity: 0 //startet transparent, wenn im Zustand 'void', also wenn es nicht da ist
        }),
        animate(300)
      ])
    ]),
    trigger('liste', [
      //von nichts zu irgendwas
      transition('void => *', [
        style({
          transform: 'translateX(-100px)'
        }),
        animate(300, style({
          transform: 'translateX(0)'
        }))
      ]),
      //Beim Löschen
      transition('* => void', group([ //Animationen gruppieren (laufen gleichzeitig)
        animate(300, style({
          color: 'red'
        })),
        animate(3000, style({
          transform: 'translateX(100%)'
        }))
      ])        
      )
    ])
  ]
})
export class AppComponent {
  title = 'animations';
  state = 'standard';
  complexState = 'standard';
  show = true;
  items = ['Äpfel', 'Birnen', 'Trauben']

  onAnimate(){
    this.state == 'standard' ? this.state = 'markiert' : this.state = 'standard';
  }
  onShrink(){
    this.state = 'kleiner';
  }
  onShow(){
    this.show = !this.show;
  }
  onNewElement(){
    this.items.push('Element ' + (this.items.length + 1));
  }
  onRemove(element){
    this.items.splice(this.items.indexOf(element), 1);
  }
  onAnimationStarted(event){
    console.log(event);
  }
  onAnimationDone(event){
    console.log(event)
  }
}
